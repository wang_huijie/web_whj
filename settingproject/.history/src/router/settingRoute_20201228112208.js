export default [
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/login/login')
    },
    {
        path: '/index',
        name: 'index',
        component: () => import('../views/setting/index'),
        children : [
            {
                path: '/users',
                name: 'users',
                component: () => import('../views/setting/users'),
                meta : { bread : ["首页","用户管理","用户列表"] }
            },
            {
                path: '/rights',
                name: 'rights',
                component: () => import('../views/setting/power'),
                meta : { bread : ["首页","权限管理","权限列表"] }
            },
            {
                path: '/roles',
                name: 'roles',
                component: () => import('../views/setting/roles'),
                meta : { bread : ["首页","权限管理","角色列表"] }
            },
            {
                path : '/categories'
            }
        ]
    },
]