import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from './utile/axios'
import ElementUI from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
Vue.use(ElementUI)

import TreeTable from "vue-table-with-tree-grid";
Vue.comp


Vue.config.productionTip = false
Vue.prototype.$axios = axios


router.beforeEach((to,from,next) => {
  let token = window.localStorage.getItem("setting_token");
  console.log(token)
  if(to.path == "/login"){
    token ? next("/index") : next()
  } else {
    token ? next() : next("/login")
  } 
  next()
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
