import axios from 'axios'

const Server = axios.create( { 
    baseURL : 'https://www.liulongbin.top:8888/api/private/v1/',
} );

Server.interceptors.request.use(function(config){
    let token = window.localStorage.getItem("setting_token");
    config["headers"].Authorization = token;
    return config
},function(error){
    return Promise.reject(error)
})

Server.interceptors.response.use(function(response){
    if(response.status == 200){
        return response.data;
    }
},function(error){
    return Promise.reject(error)
})

export default Server