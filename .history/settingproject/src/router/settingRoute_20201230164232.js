export default [
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/login/login')
    },
    {
        path: '/index',
        name: 'index',
        component: () => import('../views/setting/index'),
        children : [
            {
                path: '/users',
                name: 'users',
                component: () => import('../views/setting/users'),
                meta : { bread : ["首页","用户管理","用户列表"] }
            },
            {
                path: '/rights',
                name: 'rights',
                component: () => import('../views/setting/power'),
                meta : { bread : ["首页","权限管理","权限列表"] }
            },
            {
                path: '/roles',
                name: 'roles',
                component: () => import('../views/setting/roles'),
                meta : { bread : ["首页","权限管理","角色列表"] }
            },
            {
                path : '/categories',
                name : 'categories',
                component : () => import("../views/goods/cate"),
                meta : { bread : ["首页","商品管理","商品分类"]}
            },
            {
                path : '/params',
                name : 'params',
                component : () => import("../views/goods/params"),
                meta : { bread : ["首页","商品管理","分类参数"]}
            },
            {
                path : '/goods',
                name : 'goods',
                component : () => import("../views/goods/goodList"),
                meta : { bread : ["首页","商品管理","商品列表"]}
            },
            {
                path : '/addGoods',
                name : 'addGoods',
                component : () => import("../views/goods/addGoods"),
                meta : { bread : ["首页","商品管理","商品列表"]}
            },
            {
                path : '/orders',
                name : 'orders',
                component : () => import("../views/orders/orders"),
                meta : { bread : ["首页","商品管理","商品列表"]}
            },
            {
                path : '/reports',
                name : 'orders',
                component : () => import("../views/orders/orders"),
                meta : { bread : ["首页","商品管理","商品列表"]}
            }
        ]
    },
]