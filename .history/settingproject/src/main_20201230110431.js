import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from './utile/axios'
import ElementUI from 'element-ui'
import "element-ui/lib/theme-chalk/index.css"
Vue.use(ElementUI)

// 富文本编辑器
import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

Vue.use(VueQuillEditor, /* { default global options } */)

import TreeTable from "vue-table-with-tree-grid";
Vue.component("tree-table",TreeTable)


Vue.config.productionTip = false
Vue.prototype.$axios = axios


router.beforeEach((to,from,next) => {
  let token = window.localStorage.getItem("setting_token");
  console.log(token)
  if(to.path == "/login"){
    token ? next("/index") : next()
  } else {
    token ? next() : next("/login")
  } 
  next()
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
