import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/login')
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('../views/settings/index'),
    children: [
      {
        path: '/params',
        name: 'params',
        component: () => import('../views/categories/params')
      },
      {
        path: '/roles',
        name: 'roles',
        component: () => import('../views/role/roles')
      },
      {
        path: '/categories',
        name: 'categories',
        component: () => import('../views/categories/categories')
      },
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
