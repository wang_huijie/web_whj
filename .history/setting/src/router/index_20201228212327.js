import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
      path: '/login',
      name: 'login',
      component: () => import('../views/login/login')
  },
  {
    path: '/index',
      name: 'index',
      component: () => import('../views/setting/login')
  }
]

const router = new VueRouter({
  routes
})

export default router
