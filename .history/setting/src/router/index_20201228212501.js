import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
      path: '/login',
      name: 'login',
      component: () => import('../views/login/login')
  },
  {
    path: '/index',
      name: 'index',
      component: () => import('../views/settings/index'),
      children : [
        {
          path: '/params',
          name: 'params',
          component: () => import('../views/goods/params')
      },
      ]
  }
]

const router = new VueRouter({
  routes
})

export default router
